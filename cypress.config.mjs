import { defineConfig } from "cypress";
import coverage from "@cypress/code-coverage/task.js";

export default defineConfig({
  component: {
    specPattern: "test/*.js",
    devServer: {
      framework: "vue",
      bundler: "vite"
    },
    setupNodeEvents(on, config) {
      /* xunit override to generate separate xml reports
        (not available in mocha's internal xunit module) */
      if (config.reporter === "xunit") {
        config.reporter = "cypress/reporters/xunit.cjs";
      }
      coverage(on, config);

      return config;
    }
  }
});
