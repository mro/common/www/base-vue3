// ***********************************************************
// This example support/component.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

import "./commands";

import { mount } from "cypress/vue";
import BaseVue from "../../src/index.js";

import "@cypress/code-coverage/support";

Cypress.Commands.add("mount", (component, options = {}) => {
  options.global = options.global || {};
  options.global.plugins = options.global.plugins || [];

  options.global.plugins.push(BaseVue);

  return mount(component, options).then(({ wrapper }) => {
    cy.wrap(wrapper).as("vue");
  });
});

// Example use:
// cy.mount(MyComponent)
