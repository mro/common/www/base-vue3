// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

Cypress.Commands.add("vue", () => {
  return cy.get("@vue");
});

Cypress.Commands.add("setProps", (props) => {
  return cy.get("@vue")
  .then(async (wrapper) => {
    await wrapper.setProps(props);
    return wrapper;
  });
});
