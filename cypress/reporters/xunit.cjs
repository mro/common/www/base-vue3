
const Mocha = require("mocha");
const path = require("path");

class XUnitExt extends Mocha.reporters.XUnit {
  /**
   * @param {Mocha.Runner} runner
   * @param {Mocha.reporters.XUnit.MochaOptions} options
   */
  constructor(runner, options) {
    if (!options) { options = {}; }
    if (!options.reporterOptions) { options.reporterOptions = {}; }

    const p = path.parse(runner?.suite?.file ?? "tests");
    options.reporterOptions.output = (p.dir ?? ".") + "/results/" + p.name + ".xml";
    super(runner, options);
  }
}

module.exports = XUnitExt;
