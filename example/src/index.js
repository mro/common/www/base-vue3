
// import "./public-path";

import { createApp } from "vue";
import BaseVue from "@cern/base-vue3";
import App from "./App.vue";


const app = createApp({
  components: { App },
  template: "<App/>"
});

app.use(BaseVue, {});

app.mount("#app");
