
import path from "node:path";
import express from "express";
import pug from "pug";
import bodyParser from "body-parser";
import serveStatic from "express-static-gzip";
import { fileURLToPath } from "node:url";

const dirname = path.dirname(fileURLToPath(import.meta.url));

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

/** @type {{ basePath: string }} */
var config;
try {
  /* @ts-ignore */
  config = require('/etc/app/config'); /* eslint-disable-line global-require */
}
catch (e) {
  config = { basePath: '' };
}

var app = express();
var router = express.Router();

app.use(bodyParser.text({ type: 'text/plain' }));

router.post('/api/pugRender', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  const ret = pug.render(req.body, { pretty: (req.query.pretty === '1') });

  res.send(ret);
});

router.get('/api/error_url', ({ next }) => {
  next?.({ status: 500, message: 'Something went really wrong !' });
});

const dist = path.join(dirname, '..', 'dist');
router.use('/', serveStatic(dist, { enableBrotli: true }));

app.use(config.basePath, router);

// error handler
app.use(function(
  /** @type {any} */ err,
  /** @type {Request} */ req,
  /** @type {Response} */res,
  /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.locals.status = err.status || 500;
  res.status(err.status || 500);
  res.render('error', { baseUrl: config.basePath });
});

app.listen(process.env.PORT || 8080,
  () => console.log('Server listening on http://localhost:' + (process.env.PORT || 8080)));
