import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { default as path, resolve } from "node:path";
import istanbul from "rollup-plugin-istanbul";
import { fileURLToPath } from "node:url";
import ViteYaml from '@modyfi/vite-plugin-yaml';

const dirname = path.dirname(fileURLToPath(import.meta.url));

// https://vitejs.dev/config/

export default defineConfig(({ mode }) => {
  const config = {
    plugins: [ vue(), ViteYaml() ],
    base: '',
    root: resolve(dirname, 'src'),
    build: {
      outDir: resolve(dirname, 'dist'),
      sourcemap: true
    },
    resolve: {
      alias: {
        vue: 'vue/dist/vue.esm-bundler.js'
      }
    },
    server: {
      proxy: {
        '^/api/.*': {
          target: 'http://localhost:8080'
        }
      }
    }
  };

  if (mode === "test" || process.env.COVERAGE === "1") {
    config.plugins.push(istanbul({ include: [ "src/**/*.js" ] }));
  }
  return config;
});
