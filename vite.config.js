import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { default as path, resolve } from "node:path";
import istanbul from "rollup-plugin-istanbul";
import { fileURLToPath } from "node:url";

const dirname = path.dirname(fileURLToPath(import.meta.url));

// https://vitejs.dev/config/

export default defineConfig(({ mode }) => {
  const config = {
    plugins: [ vue() ],
    build: {
      lib: {
        entry: resolve(dirname, "src/index.js"),
        formats: [ "es" ]
      },
      sourcemap: true
    }
  };

  if (mode === "test" || process.env.COVERAGE === "1") {
    config.plugins.push(istanbul({ include: [ "src/**/*.js" ] }));
  }
  return config;
});
