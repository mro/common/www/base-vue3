
import "./scss/_light.scss";
// @ts-ignore
import * as _bootstrap from "bootstrap";

// import Vue from "vue";

import * as components from "./components";
export * from "./components";

export { default as utils } from "./utils";

/**
 * @typedef {{ auth?: boolean }} BaseVueOptions
 * @typedef {import('vue').ObjectPlugin<BaseVueOptions>} VuePlugin
 * @typedef {import('vue').App} App
 */

/** @type {VuePlugin} */
const BaseVue = {
  /**
   * @param {App} app
   * @param {BaseVueOptions} options
   */
  install(app, options) {
    // eslint-disable-next-line guard-for-in
    for (const prop in components) {
      // @ts-ignore
      app.component(prop, components[prop]);
    }

    // filters.install(Vue);
    // utils.install(Vue);

    if (options?.auth ?? false) {
      // sources.user = new UserSource(getStore());
    }
  }
};

export default BaseVue;
