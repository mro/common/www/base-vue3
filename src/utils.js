// @ts-check

import { DateTime } from "luxon";
let id = 0;

/**
 * @typedef {{ auth?: boolean }} BaseVueOptions
 * @typedef {import('vue').ObjectPlugin<BaseVueOptions>} VuePlugin
 * @typedef {import('vue').App} App
 */


/**
 * @returns {string}
 */
export function genId() {
  return "b-" + (++id);
}

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

const templateRegex = /{([^\}]*)}/g;
/**
 * @brief compile a string template
 * @details can't use "template" from lodash
 * (with { interpolate: /{([^\}]*)}/g }), since it uses eval.
 * @details uses "{word}" for parts to be replaced
 * @param  {string} source format string
 * @param  {{ [key:string]: any }} repl replacement object
 * @param  {boolean} keep whereas to keep unmatched braces
 * @return {string}
 */
export function format(source, repl, keep = false) {
  return (source ?? "").replace(templateRegex, function(match, key) {
    return repl?.[key] ?? (keep ? match : "");
  });
}

/**
 * @param {number} timestamp
 * @param {luxon.DateTimeOptions} [options]
 * @return {DateTime}
 */
export function getDateTime(timestamp, options = undefined) {
  if (timestamp > 32503676400) { /* year 3000 --> ms timestamp */
    return DateTime.fromMillis(timestamp, options);
  }
  else {
    return DateTime.fromSeconds(timestamp, options);
  }
}

export default {
  currentUrl, setCurrentUrl, format, getDateTime
};
