
import {
  PropType as VuePropType,
  Ref as VueRef } from "vue";

export = V
export as namespace V

declare namespace V {
    type PropType<T> = VuePropType<T>

    type Ref<T> = VueRef<T>
}
