import { mount } from "cypress/vue";

type MountParams = Parameters<typeof mount>
type OptionsParam = MountParams[1]

declare global {
  namespace Cypress {
    interface Chainable {
      mount: typeof mount
      setProps(props: Record<string, unknown>):
        Chainable<typeof Cypress.vueWrapper>
      vue(): Chainable<typeof Cypress.vueWrapper>
      get(obj: "@vue"): Chainable<typeof Cypress.vueWrapper>
   }
  }
}
