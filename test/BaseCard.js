// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-12T23:05:17
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { BaseCard } from "../src/index.js";

describe("<BaseCard />", () => {
  it("renders", () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(BaseCard, {
      props: { title: "my title" }
    });
    cy.get("h5").should("contain.text", "my title");
    cy.setProps({ title: "another title" });
    cy.get("h5").should("contain.text", "another title");
  });

  it("renders a body", () => {
    cy.mount(BaseCard, {
      props: { title: "my title" },
      slots: {
        header: "some header",
        default: "<li key='0'>body content</li><li key='1'>another body content</li>",
        footer: "some footer"
      },
      global: { stubs: { "transition-group": false, transition: false } }
    });
    cy.get(".card")
    .should("contain.text", "body content");

    cy.get(".card-header")
    .should("contain.text", "some header")
    .should("not.contain.text", "my title");

    cy.get(".card-footer")
    .should("contain.text", "some footer");
  });
});
