// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-12T23:05:08
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { BaseAnimationBlock } from "../src/index.js";

// describe("<BaseAnimationBlock />", () => {
//   it("can delay its removal", () => {
//     // see: https://on.cypress.io/mounting-vue
//     cy.mount({
//       data: () => ({ expand: true }),
//       template: `<BaseCollapsible :expand='expand'>
//     <template v-slot:default='c'>
//       <BaseAnimationBlock anim="delay" :appear='false' mode='' :options='{ outDelay: 0.3 }'>
//         <div v-if="c.isExpanded">body={{ c.isExpanded }}</div>
//       </BaseAnimationBlock>
//     </template>
//   </BaseCollapsible>`
//     }, {
//       stubs: { "transition-group": false, transition: false } }).as("subject");
//     cy.get("BaseAnimationBlock").should("contain.text", "body=true");

//     cy.get("@subject")

//     })
//   });

// });
